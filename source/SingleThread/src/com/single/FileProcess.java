package com.single;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileProcess {

    private static final long serialVersionUID = 1L;
    
    public String readFile(String file) {

        StringBuilder contents = new StringBuilder();
        try {
            //use buffering, reading one line at a time
            //FileReader always assumes default encoding is OK!
            BufferedReader input = new BufferedReader(new FileReader(file));
            try {
                String line = null; //not declared within while loop
        /*
                 * readLine is a bit quirky :
                 * it returns the content of a line MINUS the newline.
                 * it returns null only for the END of the stream.
                 * it returns an empty String if two newlines appear in a row.
                 */
                while ((line = input.readLine()) != null) {
                    contents.append(line);
                    contents.append(System.getProperty("line.separator"));
                }
            } finally {
                input.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return contents.toString();
    }

    public File[] getFileList(String path) {
        File file = new File(path);
        File[] files = file.listFiles();
        return files;
    }

    public void deleteFile(String filePath) {
        File fe = new File(filePath);
        if (fe.exists()) {
            fe.delete();
        }
    }

    public void moveFile(String oldfile, String newfile) {
        File f = new File(oldfile);
        File f1 = new File(newfile);
        f.renameTo(f1);
    }

    public void emptyDir(String dir) {
        File f = new File(dir);
        String[] files = f.list();
        for (int i = 0; i < files.length; i++) {
            File tmp = new File(dir + files[i].toString());
            tmp.delete();
        }
    }
}

