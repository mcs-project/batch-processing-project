/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.single;

import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.BaseFont;
import java.io.File;
import java.sql.Date;
import java.sql.Time;
import java.util.Map;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 *
 * @author sujithde
 */
public class PDFCreater {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        FontFactory.register("./resource/consola.ttf",
                "Manning");
        FontFactory.getFont("Manning", BaseFont.CP1252, BaseFont.EMBEDDED);
        FontFactory.register("./resource/consolab.ttf",
                "Manning-Bold");
        FontFactory.getFont("Manning-Bold", BaseFont.CP1252, BaseFont.EMBEDDED);

        File fc = new File("./txt/");

        String[] fNames = fc.list();
        System.out.println("No of files to process :- " + fNames.length);
        System.out.println("Process Started on " + new Date(System.currentTimeMillis()) + " at " + new Time(System.currentTimeMillis()));
        for (String fname : fNames) {
            try {
                FileProcess fp = new FileProcess();
                String mataData = fp.readFile("./txt/" + fname);

                TextProcess tp = new TextProcess();
                Map params =
                        tp.strSplitter(mataData);

                String jasper = "./resource/mc_proj.jasper";

                JasperPrint jasperPrint =
                        JasperFillManager.fillReport(jasper, params,
                        new JREmptyDataSource());

                JasperExportManager.exportReportToPdfFile(jasperPrint,
                        "./pdf/" + fname.substring(0, fname.length() - 4) + ".pdf");
                System.out.println(fname + " ..... done");
                Thread.sleep(1500);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        System.out.println("Process Finished on " + new Date(System.currentTimeMillis()) + " at " + new Time(System.currentTimeMillis()));
    }
}
