package com.single;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextProcess {

    public final static int linecount = 72;

    public String descProcessor(String desc) {

        String finalString = "";
        String block[] = desc.split("<mobile>");

        for (int i = 1; i < block.length; i++) {
            String blockDesc[] = block[i].split("<detail>");
            String mobSummary = summaryFormatter(blockDesc[0].trim());
            finalString = finalString + mobSummary;

            if (blockDesc.length == 2) {
                String mobDetail = blockDesc[1];
                String page[] = mobDetail.split("<page>");
                finalString = finalString + pageFormatter(page);
            }
        }

        if (finalString.length() != 0) {
            finalString = finalString.substring(0, finalString.length() - 3);
        }

        return finalString;
    }

    public String pageFormatter(String[] str) {
        String output = "";
        for (int j = 1; j < str.length; j++) {
            int k = lineCounter(str[j]);
            if (k < linecount) {
                int filler = linecount - k;
                for (int l = 0; l < filler + 1; l++) {
                    str[j] = str[j] + "\n";
                }
            }
            output = output + str[j];
        }
        return output;
    }

    public String summaryFormatter(String input) {
        int k = lineCounter(input);

        if (k <= linecount) {
            int filler = linecount - k;
            for (int l = 0; l < filler + 1; l++) {
                input = input + "\n";
            }
        }
        return input;
    }

    public int lineCounter(String input) {
        Pattern p;
        Matcher m;
        p = Pattern.compile("\n");
        m = p.matcher(input);
        int count = 0;
        while (m.find()) {
            count++;
        }
        return count;
    }

    public HashMap<String, Object> strSplitter(String data) {

        String str[] = data.split("###");

        String billNo = null;
        String billDate = null;
        String accNo = null;
        String ttlAmtPay = null;
        String dueDate = null;
        String vatRegNo = null;
        String addr = null;
        String balanceLstBill = null;
        String received = null;
        String balance = null;
        String ttlThisBill = null;
        String cus_name = null;
        String desc = null;
        String accSummary = null;
        String mobileNo = null;

        for (int i = 0; i < str.length; i++) {
            if (str[i].contains("Bill No.")) {
                billNo = str[i].split(":-")[1].trim();
            } else if (str[i].contains("Bill Date")) {
                billDate = str[i].split(":-")[1].trim();
            } else if (str[i].contains("Account No.")) {
                accNo = str[i].split(":-")[1].trim();
            } else if (str[i].contains("Total Amount Payable")) {
                ttlAmtPay = str[i].split(":-")[1].trim();
            } else if (str[i].contains("Settlement Date")) {
                dueDate = str[i].split(":-")[1].trim();
            } else if (str[i].contains("Your VAT Reg No")) {
                vatRegNo = str[i].split(":-")[1].trim();
            } else if (str[i].contains("Address")) {
                addr = str[i].split(":-")[1].trim();
            } else if (str[i].contains("Balance as at last bill")) {
                balanceLstBill = str[i].split(":-")[1].trim();
            } else if (str[i].contains("Received")) {
                received = str[i].split(":-")[1].trim();
            } else if (str[i].contains("Balance")) {
                balance = str[i].split(":-")[1].trim();
            } else if (str[i].contains("Total of this bill")) {
                ttlThisBill = str[i].split(":-")[1].trim();
            } else if (str[i].contains("Acc Summary")) {
                accSummary = str[i].split(":-")[1].trim();
            } else if (str[i].contains("Detail Data")) {
                desc = str[i].split(":-")[1].trim();
            } else if (str[i].contains("Customer Name")) {
                cus_name = str[i].split(":-")[1].trim();
            } else if (str[i].contains("Mob No")) {
                mobileNo = str[i].split(":-")[1].trim();
            }
        }

        HashMap<String, Object> hm = new HashMap<String, Object>();

        hm.put("bill_no", billNo);
        hm.put("bill_date", billDate);
        hm.put("acc_no", accNo);
        hm.put("total_amt_pay", ttlAmtPay);
        hm.put("due_date", dueDate);
        hm.put("vat_reg_no", vatRegNo);
        hm.put("addr", addr);
        hm.put("bal_at_last", balanceLstBill);
        hm.put("received", received);
        hm.put("balance", balance);
        hm.put("ttl_this_bill", ttlThisBill);
        hm.put("ttl_amount", ttlAmtPay);
        hm.put("cus_name", cus_name);
        hm.put("acc_summary", accSummary);
        hm.put("mob_no", mobileNo);
        hm.put("desc", formatter(descProcessor(desc)));
        return hm;
    }

    public String formatter(String str) {
        str = str.replaceAll("&", " ");
        str = str.replaceAll(":-", " ");
        return str;
    }
}
