/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.ftp;

import java.io.File;
import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;
import org.apache.ftpserver.usermanager.impl.PropertiesUserManager;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author sujithde
 */
public class FTPServer extends Thread {

    Logger  logger=null;

    public FTPServer() {
        try {
             logger=Logger.getLogger("log4j");
            PropertyConfigurator.configure("./resource/logconfig.cfg");
        } catch (Exception e) {
            System.out.println("FTPServer|" + e);
        }
    }

    @Override
    public void run() {
        try {
            FtpServerFactory serverFactory = new FtpServerFactory();
            ListenerFactory factory = new ListenerFactory();

            factory.setPort(2221);
            serverFactory.addListener("default", factory.createListener());

            PropertiesUserManagerFactory userManagerFactory = new PropertiesUserManagerFactory();
            userManagerFactory.setFile(new File("./resource/ftp.properties"));
            
            serverFactory.setUserManager(userManagerFactory.createUserManager());
            FtpServer server = serverFactory.createServer();
            
            server.start();

        } catch (FtpException ex) {
            ex.printStackTrace();
        }
    }
}
