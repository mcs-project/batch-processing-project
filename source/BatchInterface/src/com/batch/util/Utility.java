/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.util;

import com.batch.resource.IPInfo;
import com.batch.resource.ResourceObject;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.UUID;

/**
 *
 * @author sujithde
 */
public class Utility {

    public int getServerPort() {
        Properties properties = new Properties();
        FileInputStream in;
        try {
            in = new FileInputStream("./resource/app.properties");
            properties.load(in);
            in.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return Integer.valueOf(properties.getProperty("server-port"));
    }

    public int getStaticProcesingUnit() {
        Properties properties = new Properties();
        FileInputStream in;
        try {
            in = new FileInputStream("./resource/app.properties");
            properties.load(in);
            in.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return Integer.valueOf(properties.getProperty("static-proc-unit"));
    }

    public int getDymanicMemorySize() {
        Properties properties = new Properties();
        FileInputStream in;
        try {
            in = new FileInputStream("./resource/app.properties");
            properties.load(in);
            in.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return Integer.valueOf(properties.getProperty("dymanic-mem-size"));
    }

    public String getServerHost() {
        Properties properties = new Properties();
        FileInputStream in;
        try {
            in = new FileInputStream("./resource/app.properties");
            properties.load(in);
            in.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return properties.getProperty("server-host");
    }

    public String getIPAddress() {
        IPInfo ipi = new IPInfo();
        OSUtility osu = new OSUtility();
        String ip = null;

        if (osu.isWindows()) {
            ip = ipi.getWindowsIPAddress().getHostAddress();
        } else if (osu.isUnix()) {
            ip = ipi.getLinuxIPAddress();
        }
        return ip;
    }

    public String getKey() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    public int getProcessingPower(ResourceObject ro) {
        int batchSize = 0;

        int noCPU = ro.getNoCPU();
        if (noCPU > 0) {
            batchSize = noCPU;
        }
        System.out.println("No. of CPU in client " + noCPU);

        long freeMemory = ro.getFreeMemory();

        System.out.println("Client FREE memory " + freeMemory);
        Utility utility = new Utility();
        System.out.println("Configured DYNAMIC memory size " + utility.getDymanicMemorySize() * 1024);
        int x = (int) (freeMemory / (utility.getDymanicMemorySize() * 1024 * 1024));

        if (x > 0) {
            batchSize = batchSize + x;
        }
        return batchSize;
    }

    public void sleep(int sec) {
        try {
            Thread.sleep(sec);
        } catch (Exception e) {
            System.out.println("BatchServer|waitTime|" + e);
        }
    }
}
