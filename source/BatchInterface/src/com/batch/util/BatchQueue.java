/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.util;

import com.batch.manage.DataObject;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author sujithde
 */
public class BatchQueue {

    Queue<DataObject> queue = new LinkedList();

    synchronized public void pushQueue(DataObject dos) {
        queue.add(dos);
    }

    synchronized public DataObject pullQueue() {
        return queue.remove();
    }

    public int getQueueSize() {
        return queue.size();
    }

    public boolean isEmpty() {
        if (queue.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
