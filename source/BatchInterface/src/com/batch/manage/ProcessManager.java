/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.manage;

import com.batch.client.BatchClientInterface;
import com.batch.sec.BatchSecurityManager;
import com.batch.type.SecurityType;
import com.batch.server.BatchServer;
import com.batch.server.ClientManager;
import com.batch.type.DataType;
import com.batch.type.ServerStatusType;
import com.batch.type.StatusType;
import com.batch.util.BatchQueue;
import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author sujithde
 */
public class ProcessManager extends Thread {

    BatchRequest batchRequest = null;
    ClientManager cm = null;
    BatchQueue batchQueue = null;
    DataManager dm = null;

    public ProcessManager(BatchRequest batchRequest, ClientManager cm, BatchQueue batchQueue, DataManager dm) {
        this.batchRequest = batchRequest;
        this.cm = cm;
        this.batchQueue = batchQueue;
        this.dm = dm;
        System.out.println("process manager is starting...");
    }

    @Override
    public void run() {
        System.out.println("process manager is started.");
        BatchClientInterface bci = batchRequest.getBci();
        try {

            if (batchRequest.getSecurityType() == SecurityType.FULL) {
                BatchSecurityManager bsm = new BatchSecurityManager();
                byte[] obytes = bsm.getBytes(batchRequest);
                char[] encrypted = bsm.encrypt(obytes);
                byte[] response = bsm.decrypt(bci.processData(encrypted, SecurityType.FULL));

                Object object = bsm.getObject(response);
                BatchResponse batchResponse = (BatchResponse) object;

                Collection<DataObject> coldo = batchResponse.getCol();

                Iterator<DataObject> itrdo = coldo.iterator();
                while (itrdo.hasNext()) {
                    DataObject dataObject = itrdo.next();
                    dm.addQueue(DataType.OUTPUT, dataObject);
                }
               
                BatchServer batchServer = new BatchServer();
                batchServer.finishProcess(bci);
                ServerStatusType.status = ServerStatusType.DONE;

            } else if (batchRequest.getSecurityType() == SecurityType.PARTIAL) {
                BatchSecurityManager bsm = new BatchSecurityManager();
                byte[] obytes = bsm.getBytes(batchRequest);
                char[] encrypted = bsm.encodeHex(obytes);
                byte[] response = bsm.decodeHex(bci.processData(encrypted, SecurityType.PARTIAL));

                Object object = bsm.getObject(response);
                BatchResponse batchResponse = (BatchResponse) object;

                Collection<DataObject> coldo = batchResponse.getCol();
                Iterator<DataObject> itrdo = coldo.iterator();
                while (itrdo.hasNext()) {
                    DataObject dataObject = itrdo.next();
                    dm.addQueue(DataType.OUTPUT, dataObject);
                }

                BatchServer batchServer = new BatchServer();
                batchServer.finishProcess(bci);
                ServerStatusType.status = ServerStatusType.DONE;

            } else {
                BatchResponse batchResponse = bci.processData(batchRequest, SecurityType.NONE);
                Collection<DataObject> coldo = batchResponse.getCol();
                Iterator<DataObject> itrdo = coldo.iterator();
                while (itrdo.hasNext()) {
                    DataObject dataObject = itrdo.next();
                    dm.addQueue(DataType.OUTPUT, dataObject);
                }
                BatchServer batchServer = new BatchServer();
                batchServer.finishProcess(bci);
                ServerStatusType.status = ServerStatusType.DONE;
            }
        } catch (Exception e) {
            
            cm.remove(bci, StatusType.PROCESSING);

            System.out.println("Client Killed. Reloading Data to Batch Server ...");
            Collection<DataObject> cole = batchRequest.getCol();
            Iterator<DataObject> itre = cole.iterator();

            while (itre.hasNext()) {
                DataObject dataObject = itre.next();
                batchQueue.pushQueue(dataObject);
            }
            System.out.println("Reloading .... SUCCESS");
            System.out.println("New batch queue size|" + batchQueue.getQueueSize());
        }
    }
}
