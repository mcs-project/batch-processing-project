/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.manage;

import com.batch.client.BatchClientInterface;
import com.batch.resource.ResourceObject;
import com.batch.type.SecurityType;
import com.batch.server.ClientManager;
import com.batch.client.BatchClientLogic;
import com.batch.server.BatchServer;
import com.batch.type.AlgorithmType;
import com.batch.type.ClientStatusType;
import com.batch.type.DataType;
import com.batch.type.ServerStatusType;
import com.batch.type.StatusType;
import com.batch.util.BatchQueue;
import com.batch.util.Status;
import com.batch.util.Utility;
import java.rmi.RemoteException;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author sujithde
 */
public class CommunicationManager extends Thread {

    BatchQueue batchQueue = null;
    int batchAlgo = 0;
    BatchClientLogic pl = null;
    int secType = -1;
    ClientManager cm = null;
    int staticProcUnit = 0;
    DataManager dm = null;
    boolean printTime = false;

    public CommunicationManager() throws RemoteException {
    }

    public CommunicationManager(int batchAlgo, int secType, ClientManager cm, BatchClientLogic pl, BatchQueue batchQueue, DataManager dm) throws RemoteException {
        this.batchQueue = batchQueue;
        this.pl = pl;
        this.secType = secType;
        this.cm = cm;

        if (batchAlgo == AlgorithmType.ROUND_BOBIN) {
            this.batchAlgo = batchAlgo;
            System.out.println("Algorithm is set to ROUND_BOBIN");
        } else if (batchAlgo == AlgorithmType.DYNAMIC) {
            this.batchAlgo = batchAlgo;
            System.out.println("Algorithm is set to DYNAMIC");

        } else if (batchAlgo == AlgorithmType.STATIC) {
            this.batchAlgo = batchAlgo;
            System.out.println("Algorithm is set to STATIC");

        } else {
            System.out.println("No batch proccessing algorithm found");
            this.batchAlgo = AlgorithmType.ROUND_BOBIN;
            System.out.println("Algorithm is set to (default)ROUND_BOBIN");

        }

        if (secType == SecurityType.FULL) {
            System.out.println("Security type is set to FULL");
        } else if (secType == SecurityType.PARTIAL) {
            System.out.println("Security type is set to PARTIAL");
        } else if (secType == SecurityType.NONE) {
            System.out.println("Security type is set to NONE");
        } else {
            System.out.println("Security type is set to NONE");
        }

        Utility utility = new Utility();
        staticProcUnit = utility.getStaticProcesingUnit();

        this.dm = dm;
    }

    @Override
    public void run() {

        while (true) {
            CopyOnWriteArrayList<BatchClientInterface> colidle = cm.getIDLEClientList();
            Iterator<BatchClientInterface> itridle = colidle.iterator();

            if (!batchQueue.isEmpty()) {
                if (colidle.size() > 0) {

                    if (StatusType.status != StatusType.PROCESSING) {
                        StatusType.status = StatusType.PROCESSING;
                    }

                    if (!printTime) {
                        Date date = new Date(System.currentTimeMillis());
                        Time time = new Time(System.currentTimeMillis());
                        System.out.println("Processing Started on " + date + " at " + time + " (" + System.currentTimeMillis() + ")");
                        printTime = true;
                    }

                    try {
                        sleep(10);
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("CommunicationManager|" + e);
                    }

                    while (itridle.hasNext()) {
                        BatchClientInterface bci = itridle.next();
                        try {
                            String clientID = bci.getClientID();
                            String status = bci.getIDLEStatus();
                            System.out.println(clientID + status);
                            cm.remove(bci, Status.IDLE);
                            cm.add(bci, Status.ACTIVE);

                            System.out.println();
                            System.out.println(clientID + " state changed to IDLE to ACTIVE");
                            System.out.print("Loading Processing Logic to client(" + clientID + ") ..... ");
                            bci.setProcessLogic(pl);
                            System.out.println("SUCCESS");

                        } catch (Exception e) {
                            e.printStackTrace();
                            System.out.println("CommunicationManager|" + e);
                        }
                    }
                }
            } else {
                colidle = null;
            }

            CopyOnWriteArrayList<BatchClientInterface> colactive = cm.getActiveClientList();
            Iterator<BatchClientInterface> itractive = colactive.iterator();

            if (colactive.size() > 0) {

                if (batchAlgo == AlgorithmType.ROUND_BOBIN) {
                    while (itractive.hasNext()) {
                        BatchClientInterface bci = itractive.next();
                        try {
                            if (!batchQueue.isEmpty()) {

                                cm.remove(bci, Status.ACTIVE);
                                cm.add(bci, Status.PROCESSING);

                                String clientID = bci.getClientID();
                                System.out.println(clientID + " state changed to ACTIVE to PROCESSING.");
                                System.out.println();

                                DataObject dos = batchQueue.pullQueue();

                                BatchRequest br = new BatchRequest();
                                Utility utility = new Utility();
                                br.setKey(utility.getKey());
                                Collection<DataObject> colreq = new ArrayList<DataObject>();
                                colreq.add(dos);
                                br.setCol(colreq);
                                br.setSecurityType(secType);
                                br.setBci(bci);

                                new ProcessManager(br, cm, batchQueue, dm).start();
                            } else {
                                ServerStatusType.status = ServerStatusType.FINISH;
                                bci.setClientStatus(ClientStatusType.IDLE);
                            }
                        } catch (Exception e) {
                            cm.remove(bci, Status.IDLE);
                            cm.remove(bci, Status.ACTIVE);
                            cm.remove(bci, Status.PROCESSING);
                            System.out.println("CommunicationManager|" + e);
                        }
                    }
                } else if (batchAlgo == AlgorithmType.STATIC) {
                    while (itractive.hasNext()) {
                        BatchClientInterface bci = itractive.next();
                        try {
                            if (!batchQueue.isEmpty()) {

                                cm.remove(bci, Status.ACTIVE);
                                cm.add(bci, Status.PROCESSING);

                                String clientID = bci.getClientID();
                                System.out.println(clientID + " state changed to ACTIVE to PROCESSING.");
                                System.out.println();

                                BatchRequest br = new BatchRequest();
                                Utility utility = new Utility();
                                br.setKey(utility.getKey());
                                Collection<DataObject> colreq = new ArrayList<DataObject>();

                                int size = batchQueue.getQueueSize();

                                int x = 0;

                                if (size > staticProcUnit) {
                                    x = staticProcUnit;
                                } else {
                                    x = size;
                                }

                                for (int i = 0; i < x; i++) {
                                    DataObject dos = batchQueue.pullQueue();
                                    colreq.add(dos);
                                }

                                br.setCol(colreq);
                                br.setSecurityType(secType);
                                br.setBci(bci);

                                new ProcessManager(br, cm, batchQueue, dm).start();
                            } else {
                                ServerStatusType.status = ServerStatusType.FINISH;
                                bci.setClientStatus(ClientStatusType.IDLE);
                            }
                        } catch (Exception e) {
                            cm.remove(bci, Status.IDLE);
                            cm.remove(bci, Status.ACTIVE);
                            cm.remove(bci, Status.PROCESSING);
                            System.out.println("CommunicationManager|" + e);
                        }
                    }
                } else if (batchAlgo == AlgorithmType.DYNAMIC) {
                    while (itractive.hasNext()) {
                        BatchClientInterface bci = null;
                        try {
                            bci = itractive.next();
                            if (!batchQueue.isEmpty()) {

                                String clientID = bci.getClientID();
                                System.out.print("Requesting " + clientID + " rource uilization .....");
                                ResourceObject ro = bci.getResourceUtlizationObject();
                                System.out.println("SUCCESS");

                                if (ro != null) {
                                    System.out.println("Calculating " + clientID + " processing power ..... ");

                                    Utility utility = new Utility();
                                    int power = utility.getProcessingPower(ro);

                                    System.out.println(clientID + " Dymanic Batch size ..... " + power);

                                    cm.remove(bci, Status.ACTIVE);
                                    cm.add(bci, Status.PROCESSING);

                                    System.out.println(clientID + " state changed to ACTIVE to PROCESSING");
                                    System.out.println();

                                    BatchRequest br = new BatchRequest();
                                    br.setKey(utility.getKey());
                                    Collection<DataObject> colreq = new ArrayList<DataObject>();

                                    int size = batchQueue.getQueueSize();

                                    int x = 0;

                                    if (size > power) {
                                        x = power;
                                    } else {
                                        x = size;
                                    }

                                    for (int i = 0; i < x; i++) {
                                        DataObject dos = batchQueue.pullQueue();
                                        colreq.add(dos);
                                    }

                                    System.out.println("Current batch queue size|" + batchQueue.getQueueSize());
                                    br.setCol(colreq);
                                    br.setSecurityType(secType);
                                    br.setBci(bci);

                                    new ProcessManager(br, cm, batchQueue, dm).start();
                                }
                            } else {
                                ServerStatusType.status = ServerStatusType.FINISH;
                                bci.setClientStatus(ClientStatusType.IDLE);
                            }
                        } catch (Exception e) {
                            cm.remove(bci, Status.IDLE);
                            cm.remove(bci, Status.ACTIVE);
                            cm.remove(bci, Status.PROCESSING);
                            System.out.println("CommunicationManager|" + e);
                        }
                    }
                }
            }
        }
    }
}
