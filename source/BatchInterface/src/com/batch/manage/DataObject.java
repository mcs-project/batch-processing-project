/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.manage;

import com.batch.util.Utility;
import java.io.Serializable;

/**
 *
 * @author sujithde
 */
public class DataObject implements Serializable {

    public String key = null;
    public Object value = null;

    public DataObject() {
        Utility u = new Utility();
        setKey(u.getKey());
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
