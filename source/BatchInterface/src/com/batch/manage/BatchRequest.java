/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.manage;

import com.batch.client.BatchClientInterface;
import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author sujithde
 */
public class BatchRequest implements Serializable{

    public String key = null;
    public Collection<DataObject> col = null;
    public BatchClientInterface bci = null;
    public int securityType = 0;

    public int getSecurityType() {
        return securityType;
    }

    public void setSecurityType(int securityType) {
        this.securityType = securityType;
    }

    public BatchClientInterface getBci() {
        return bci;
    }

    public void setBci(BatchClientInterface bci) {
        this.bci = bci;
    }

    public String getKey() {
        return key;
    }

    public Collection<DataObject> getCol() {
        return col;
    }

    public void setCol(Collection<DataObject> col) {
        this.col = col;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
