/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.manage;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author sujithde
 */
public class BatchResponse implements Serializable{

    public String key = null;
    public Collection<DataObject> col = null;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Collection<DataObject> getCol() {
        return col;
    }

    public void setCol(Collection<DataObject> col) {
        this.col = col;
    }
}
