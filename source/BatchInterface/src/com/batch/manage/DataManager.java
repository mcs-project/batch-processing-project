/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.manage;

import com.batch.type.DataType;
import com.batch.type.ServerStatusType;
import java.rmi.RemoteException;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author sujithde
 */
public class DataManager extends Thread {

    private static CopyOnWriteArrayList<DataObject> input =
            new CopyOnWriteArrayList<DataObject>();
    private static CopyOnWriteArrayList<DataObject> output =
            new CopyOnWriteArrayList<DataObject>();

    public DataManager() throws RemoteException {
    }

    public synchronized void addQueue(int dataType, DataObject dataObject) {
        if (dataType == DataType.INPUT) {
            input.add(dataObject);
        } else if (dataType == DataType.OUTPUT) {
            output.add(dataObject);
        }
    }

    public int getQueueSize(int dataType) {
        int size = 0;
        if (dataType == DataType.INPUT) {
            size = input.size();
        } else if (dataType == DataType.OUTPUT) {
            size = output.size();
        }
        return size;
    }

    public synchronized CopyOnWriteArrayList<DataObject> getQueue(int dataType) {
        CopyOnWriteArrayList<DataObject> rtnArray = new CopyOnWriteArrayList<DataObject>();
        if (dataType == DataType.INPUT) {
            rtnArray = input;
        } else if (dataType == DataType.OUTPUT) {
            int size = output.size();
            for (int i = 0; i < size; i++) {
                DataObject dataObject = output.get(i);
                rtnArray.add(dataObject);
            }
        }
        return rtnArray;
    }

    @Override
    public void run() {

        while (true) {

            CopyOnWriteArrayList<DataObject> colin = getQueue(DataType.INPUT);
            CopyOnWriteArrayList<DataObject> colout = getQueue(DataType.OUTPUT);

            if (colout.size() > 0) {
                if (colin.size() == colout.size()) {
                    ServerStatusType.status = ServerStatusType.DONE;
                }
            }
        }
    }
}
