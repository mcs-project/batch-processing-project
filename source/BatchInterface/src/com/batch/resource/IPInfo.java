/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.resource;

import java.net.*;
import java.util.Enumeration;

/**
 *
 * @author SUJITHDE
 */
public class IPInfo {

    public InetAddress getWindowsIPAddress() {
        InetAddress ip = null;
        try {
            ip = InetAddress.getLocalHost();
        } catch (Exception e) {
            System.out.println("getWindowsIPAddress|" + e.getMessage());
        }
        return ip;
    }

    public String getLinuxIPAddress() {
        String lip = null;
        try {
            NetworkInterface ni = NetworkInterface.getByName("eth0");
            Enumeration<InetAddress> inetAddresses = ni.getInetAddresses();

            while (inetAddresses.hasMoreElements()) {
                InetAddress ia = inetAddresses.nextElement();
                if (!ia.isLinkLocalAddress()) {
                    lip = ia.getHostAddress();
                }
            }
        } catch (SocketException ex) {
            System.out.println("getLinuxIPAddress|" + ex.getMessage());
        }
        return lip;
    }
}
