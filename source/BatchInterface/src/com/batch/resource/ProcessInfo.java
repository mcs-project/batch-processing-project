/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.batch.resource;

import java.util.Map;
import org.hyperic.sigar.ProcStat;
import org.hyperic.sigar.ProcState;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;

/**
 *
 * @author SUJITHDE
 */
public class ProcessInfo {

    private static Sigar sigar;

    public ProcessInfo(Sigar s) throws SigarException {
        this.sigar = s;
        System.out.println(getMetric().toString());
        System.out.println(getMetric(getPidString()).toString());
    }

    public static void main(String[] args) throws SigarException {
        new ProcessInfo(new Sigar());
        System.out.println(ProcessInfo.getMetric());
        System.out.println(ProcessInfo.getMetric(getPidString()));

        for(int i=0;i<1000000;i++)
        System.out.println("ID : "+getPid());
    }

    public static Map<String, String> getMetric() throws SigarException {
        ProcStat state = sigar.getProcStat();
        return (Map<String, String>) state.toMap();
    }

    public static Map<String, String> getMetric(String pid) throws SigarException {
        ProcState state = sigar.getProcState(pid);
        return (Map<String, String>) state.toMap();
    }

    public static long getPid() {
        return sigar.getPid();
    }

    public static String getPidString() {
        return ""+sigar.getPid();
    }
}