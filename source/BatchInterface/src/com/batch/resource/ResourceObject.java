/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.resource;

import java.io.Serializable;

/**
 *
 * @author sujithde
 */
public class ResourceObject implements Serializable{

    private static final long serialVersionUID = 1L;
    private int noCPU = 0;
    private double cpuIDLE = 0;
    private long cpuCache = 0;
    private long totalMemory = 0;
    private long freeMemory = 0;
    private double freeMemoryPercent = 0;

    public double getFreeMemoryPercent() {
        return freeMemoryPercent;
    }

    public void setFreeMemoryPercent(double freeMemoryPercent) {
        this.freeMemoryPercent = freeMemoryPercent;
    }

    public long getCpuCache() {
        return cpuCache;
    }

    public void setCpuCache(long cpuCache) {
        this.cpuCache = cpuCache;
    }

    public int getNoCPU() {
        return noCPU;
    }

    public void setNoCPU(int noCPU) {
        this.noCPU = noCPU;
    }

    public double getCpuIDLE() {
        return cpuIDLE;
    }

    public void setCpuIDLE(double cpuIDLE) {
        this.cpuIDLE = cpuIDLE;
    }

    public long getTotalMemory() {
        return totalMemory;
    }

    public void setTotalMemory(long totalMemory) {
        this.totalMemory = totalMemory;
    }

    public long getFreeMemory() {
        return freeMemory;
    }

    public void setFreeMemory(long freeMemory) {
        this.freeMemory = freeMemory;
    }
}
