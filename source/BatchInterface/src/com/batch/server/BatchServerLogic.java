/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.server;

import com.batch.manage.DataManager;
import com.batch.manage.DataObject;
import com.batch.type.DataType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author sujithde
 */
public abstract class BatchServerLogic implements Serializable{

    public abstract Collection<DataObject> preProcess();

    public abstract boolean postProcess(Collection<DataObject> col);

    public Collection<DataObject> getOutput() {
        Collection<DataObject> colout = new ArrayList<DataObject>();
        try {
            DataManager dm = new DataManager();
            colout = dm.getQueue(DataType.OUTPUT);
        } catch (Exception e) {
            System.out.println("getOutput|" + e);
        }
        return colout;
    }
}
