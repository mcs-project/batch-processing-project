/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.server;

import com.batch.sec.SecurityObject;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author sujithde
 */
public class TestServer {

    public static void main(String[] args) throws RemoteException {

        BatchServer rmis = new BatchServer();
        ServerProcess sp = new ServerProcess() {
            @Override
            public String process() {
                return "1223";
            }

            @Override
            public Collection setBatchData() {
                Collection<SecurityObject> col = new ArrayList<SecurityObject>();

                for (int i = 0; i < 5; i++) {
                    SecurityObject dao = new SecurityObject();
                    dao.setId("ID" + i);
                    dao.setName("Name :- " + i);
                    col.add(dao);
                }

                return col;
            }
        };
//        rmis.startServer(sp);
    }
}
