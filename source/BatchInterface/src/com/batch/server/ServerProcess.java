/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.server;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author sujithde
 */
public abstract class ServerProcess  {

        private static final long serialVersionUID = 1L;
    public abstract String process();

    public abstract Collection setBatchData();
}
