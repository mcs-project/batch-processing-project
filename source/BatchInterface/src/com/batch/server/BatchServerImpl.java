/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.server;

import com.batch.client.BatchClientLogic;
import com.batch.manage.BatchRequest;
import com.batch.manage.BatchResponse;
import com.batch.manage.DataObject;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author sujithde
 */
public class BatchServerImpl implements Serializable{
    
  public static void main(String[] args) throws RemoteException, MalformedURLException {

        BatchServerLogic bsl = new BatchServerLogic() {
            @Override
            public Collection<DataObject> preProcess() {
                Collection<DataObject> col = new ArrayList<DataObject>();
                DataObject do1 = new DataObject();
                do1.setValue("111");
                DataObject do2 = new DataObject();
                do2.setValue("222");
                DataObject do3 = new DataObject();
                do3.setValue("333");
                DataObject do4 = new DataObject();
                do4.setValue("444");
                DataObject do5 = new DataObject();
                do5.setValue("555");
                DataObject do6 = new DataObject();
                do6.setValue("666");
                DataObject do7 = new DataObject();
                do7.setValue("777");
                DataObject do8 = new DataObject();
                do8.setValue("888");
                col.add(do1);
                col.add(do2);
                col.add(do3);
                col.add(do4);
                col.add(do5);
                col.add(do6);
                col.add(do7);
                col.add(do8);
                return col;
            }

            @Override
            public boolean postProcess(Collection<DataObject> col) {
                Iterator<DataObject> colpoos = col.iterator();

                while (colpoos.hasNext()) {
                    DataObject dos = colpoos.next();

                    System.out.println(dos.getKey());
                    System.out.println(dos.getValue());
                }
                System.out.println("postProcess " + col.size());
                return true;
            }
        };

        BatchClientLogic bcl = new BatchClientLogic() {
            @Override
            public BatchResponse doProcess(BatchRequest batchRequest) {

                Collection<DataObject> breq = batchRequest.getCol();
                Iterator<DataObject> itr = breq.iterator();

                BatchResponse br = new BatchResponse();
                Collection<DataObject> col = new ArrayList<DataObject>();

                String str = "";
                while (itr.hasNext()) {
                    DataObject dos = itr.next();
                    String s = dos.getValue().toString() + "sujith the best";
                    System.out.println(s);
                    dos.setValue(s);
                    col.add(dos);
                }

                br.setCol(col);
                br.setKey(batchRequest.getKey());
                return br;
            }
        };
        BatchServer batchServer = new BatchServer();
        batchServer.startServer(3, 2, bsl, bcl);
    }

}
