package com.batch.server;

import com.batch.client.BatchClientInterface;
import com.batch.client.BatchClientLogic;
import com.batch.ftp.FTPServer;
import com.batch.manage.CommunicationManager;
import com.batch.manage.DataManager;
import com.batch.manage.DataObject;
import com.batch.manage.ProcessManager;
import com.batch.type.AlgorithmType;
import com.batch.type.DataType;
import com.batch.type.ServerStatusType;
import com.batch.type.StatusType;
import com.batch.util.BatchQueue;
import com.batch.util.Status;
import com.batch.util.Utility;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Time;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class BatchServer implements BatchServerInterface {

    private static final long serialVersionUID = 1L;
    private ClientManager clientManager = new ClientManager();
    static ServerProcess sp = null;
    static BatchQueue bq = null;

    public BatchServer() throws RemoteException {
    }

    @Override
    public void joinIDLE(BatchClientInterface bci) throws RemoteException {
        String clientID = bci.getClientID();
        System.out.print("Authenticating client(" + clientID + ") request ..... ");

        if (clientID != null) {
            System.out.println("SUCCESS");
            System.out.println("Connecting to client(" + clientID + ") ..... ");
            clientManager.add(bci, Status.IDLE);
            System.out.println("Connected to client(" + clientID + ") ..... SUCCESS");
        } else {
            System.out.println("Authentication failed. Force termination client.");
            bci.forceTerminate();
        }
    }

    @Override
    public boolean isServerActive() throws RemoteException {
        if (ServerStatusType.status == ServerStatusType.ACTIVE || StatusType.status == StatusType.PROCESSING || ServerStatusType.status == ServerStatusType.DONE) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String joinRequest(BatchClientInterface bci) throws RemoteException {
        String clientID = bci.getClientID();
        return "hello, " + clientID;
    }

    @Override
    public synchronized void leaveProcess(String clientID) throws RemoteException {
//        clientManager.remove(clientID, Status.IDLE);
//        clientManager.remove(clientID, Status.ACTIVE);
//        System.out.println(clientID + " left the batch server success.");
    }

    public void finishProcess(BatchClientInterface bci) throws RemoteException {
        clientManager.remove(bci, Status.PROCESSING);
        clientManager.add(bci, Status.ACTIVE);
    }

    @Override
    public Collection sendProcessedData(String clientID) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    static BatchServer server = null;

    public void startServer(int batchAlgorithm, int securityType, BatchServerLogic bsl, BatchClientLogic bcl) {
        try {
            ServerStatusType.status = ServerStatusType.INIT;

            System.out.println("Initializing batch server environment ..... ");
            System.out.println("System status set to INIT");

            System.out.print("Loading server Policy file ..... ");
            System.setProperty("java.security.policy", "./resource/policy.cfg");
            System.out.println("SUCCESS");

            System.out.print("Setting up the Security Manager ..... ");
            System.setSecurityManager(new RMISecurityManager());
            System.out.println("SUCCESS");

            Utility utility = new Utility();
            String serverHost = utility.getServerHost();
            System.setProperty("java.rmi.server.hostname", serverHost);
            System.out.println("Initializing ..... SUCCESS");

            System.out.println();
            System.out.println("System status set to IDLE");
            System.out.println();

            Registry reg = LocateRegistry.createRegistry(1099);
            server = new BatchServer();
            BatchServerInterface bsi = (BatchServerInterface) UnicastRemoteObject.exportObject(server, 0);
            reg.rebind("batchproc", bsi);

            utility.sleep(1000);
            System.out.print("Starting FTP Server ..... ");
            FTPServer fTPServer = new FTPServer();
            fTPServer.start();
            System.out.println("SUCCESS");

            System.out.println();
            System.out.println("Data manipulating starting ..... ");

            DataManager dataManager = new DataManager();
            Collection<DataObject> colbsl = bsl.preProcess();
            Iterator<DataObject> itrbsl = colbsl.iterator();

            while (itrbsl.hasNext()) {
                DataObject dataObject = itrbsl.next();
                dataManager.addQueue(DataType.INPUT, dataObject);
            }

            if (colbsl != null) {
                if (!colbsl.isEmpty()) {

                    bq = new BatchQueue();

                    if (batchAlgorithm == AlgorithmType.ROUND_BOBIN || batchAlgorithm == AlgorithmType.STATIC || batchAlgorithm == AlgorithmType.DYNAMIC) {
                        DataObject[] dataObjects = (DataObject[]) colbsl.toArray(new DataObject[0]);

                        for (int i = 0; i < dataObjects.length; i++) {
                            bq.pushQueue(dataObjects[i]);
                        }

                        System.out.println("Batch size ..... " + colbsl.size());
                        System.out.println("Data manipulating ..... SUCCESS");
                        System.out.println();
                        CommunicationManager communicationManager = new CommunicationManager(batchAlgorithm, securityType, clientManager, bcl, bq, dataManager);
                        utility.sleep(1000);
                        System.out.println();
                        System.out.print("Starting Communication Manager ..... ");
                        communicationManager.start();
                        System.out.println("SUCCESS");
                        System.out.println("Starting Data Manager ..... SUCCESS");
                        dataManager.start();

                        if (!(ProcessManager.activeCount() > 0 && CommunicationManager.activeCount() > 0)) {
                            System.out.println("Batch server failed start due to threads are not working.");
                            System.exit(0);
                        } else {
                            ServerStatusType.status = ServerStatusType.ACTIVE;
                            System.out.println("System status set to ACTIVE");
                            System.out.println();
                            System.out.println("Waiting for client requests...");
                            System.out.println();
                        }

                        utility.sleep(1000);

                        while (true) {
                            boolean finish = false;
                            if (isClientFinish()) {
                                CopyOnWriteArrayList<DataObject> coldone = dataManager.getQueue(DataType.OUTPUT);
                                if (coldone.size() > 0) {
                                    if (bsl.postProcess(coldone)) {
                                        finish = true;
                                    }
                                }
                            }

                            boolean running = false;

                            if (ServerStatusType.status == ServerStatusType.FINISH) {
                                int isize = dataManager.getQueueSize(DataType.INPUT);
                                int osize = dataManager.getQueueSize(DataType.OUTPUT);

                                if (ProcessManager.activeCount() <= 4 && bq.getQueueSize() == 0 && isize == osize) {
                                    if (finish) {
                                        running = true;
                                    }
                                }
                            }

                            if (running) {
                                break;
                            }
                        }

                        java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
                        Time time = new Time(System.currentTimeMillis());
                        System.out.println("Processing Finished on " + date + " at " + time + " (" + System.currentTimeMillis() + ")");

                        System.out.println("Batch Processing SUCCESSFULLY Finished.");
                        System.exit(0);

                    } else {
                        System.out.println("No batch algorithm found");
                        System.exit(0);
                    }
                } else {
                    System.out.println("No batches of data found to process");
                    System.exit(0);
                }
            } else {
                System.exit(0);
                System.out.println("No batches of data found to process");
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            System.out.println("Communication error " + e.toString());
        }
    }

    public boolean isClientFinish() {
        boolean done = false;
        if (ServerStatusType.status == ServerStatusType.DONE) {
            done = true;
        }
        return done;
    }

    @Override
    public int getServerStatus() throws RemoteException {
        return ServerStatusType.status;
    }

    @Override
    public int getBatchSize() throws RemoteException {
        return bq.getQueueSize();
    }
}
