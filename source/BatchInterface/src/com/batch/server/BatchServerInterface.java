package com.batch.server;

import com.batch.client.BatchClientInterface;
import java.rmi.*;
import java.util.Collection;

public interface BatchServerInterface extends Remote {

    public boolean isServerActive() throws RemoteException;

    public String joinRequest(BatchClientInterface bci) throws RemoteException;

    public void joinIDLE(BatchClientInterface bci) throws RemoteException;

    public void leaveProcess(String clientID) throws RemoteException;

    public Collection sendProcessedData(String clientID) throws RemoteException;

    public int getServerStatus() throws RemoteException;
    
    public int getBatchSize() throws RemoteException;
}
