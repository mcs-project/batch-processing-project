package com.batch.server;

import com.batch.client.BatchClientInterface;
import com.batch.util.Status;
import java.util.concurrent.CopyOnWriteArrayList;

public class ClientManager {

    private static CopyOnWriteArrayList<BatchClientInterface> activeClient = new CopyOnWriteArrayList<BatchClientInterface>();
    private static CopyOnWriteArrayList<BatchClientInterface> idleClient = new CopyOnWriteArrayList<BatchClientInterface>();
    private static CopyOnWriteArrayList<BatchClientInterface> processClient = new CopyOnWriteArrayList<BatchClientInterface>();
    private static int counter = 0;

    public synchronized boolean add(BatchClientInterface bci, int status) {

        boolean addition = false;
        try {
            while (counter > 0) {
                wait();
            }

            if (status == Status.IDLE) {
                addition = idleClient.add(bci);
            } else if (status == Status.ACTIVE) {
                addition = activeClient.add(bci);
            } else if (status == Status.PROCESSING) {
                addition = processClient.add(bci);
            }
        } catch (InterruptedException e) {
            System.out.println("ClientManager|client addition interrupted.|" + e);
        } finally {
            notifyAll();
        }
        System.out.println("IDLE Client(" + idleClient.size() + "), Active Client(" + activeClient.size() + "), Processing Client(" + processClient.size() + ")");
        return addition;
    }

    public synchronized boolean remove(BatchClientInterface bci, int status) {

        boolean remove = false;
        try {
            while (counter > 0) {
                wait();
            }

            if (status == Status.IDLE) {
                remove = idleClient.remove(bci);
            } else if (status == Status.ACTIVE) {
                remove = activeClient.remove(bci);
            } else if (status == Status.PROCESSING) {
                remove = processClient.remove(bci);
            }
        } catch (InterruptedException e) {
            System.out.println("ClientManager|remove client interrupted.");
        } finally {
            notifyAll();
        }
        return remove;
    }

    public synchronized void incCounter() {
        counter++;
        notifyAll();
    }

    public synchronized void decCounter() {
        counter--;
        notifyAll();
    }

    public CopyOnWriteArrayList<BatchClientInterface> getActiveClientList() {
        return activeClient;
    }

    public CopyOnWriteArrayList<BatchClientInterface> getIDLEClientList() {
        return idleClient;
    }

    public CopyOnWriteArrayList<BatchClientInterface> getProcessClientList() {
        return processClient;
    }
}
