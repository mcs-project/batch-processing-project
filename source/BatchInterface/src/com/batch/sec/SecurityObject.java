/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.sec;

import java.io.Serializable;

/**
 *
 * @author sujithde
 */
public class SecurityObject {
    
        private static final long serialVersionUID = 1L;
    private String id=null;
    private String name=null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
