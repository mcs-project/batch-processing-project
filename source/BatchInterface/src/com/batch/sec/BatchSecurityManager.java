package com.batch.sec;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

/**
 *
 * @author sujithde
 */
public class BatchSecurityManager {

    Cipher cipher = null;
    KeyPair keypair = null;
    PublicKey pubKey = null;
    PrivateKey priKey = null;

    public BatchSecurityManager() throws Exception {

        cipher = Cipher.getInstance("RSA");

        byte[] expBytes = Base64.decodeBase64("AQAB");
        byte[] modBytes = Base64.decodeBase64("nzLiZDSiu484r5NcBQN3rNP3x5aqY3Eq6CkQDwuilTzd5ZNdTcTxw7C1JQ9ih27Vq4RU9NYgi9oOUTVQ2gkqP1OJA9aawjCRwMJ7PRyKlBEpsdE/wFtu9/1ciGRtWSyACr2jTASZPQa+aHQh2qziacWd+iVmGIq0+l11nGG/GYU=");
        byte[] dBytes = Base64.decodeBase64("nyF45NssUzkdW3t7/tLxfENBKTN0TARh9ECfebqSoIR/9awxFrynQYnP+CSBw4jJcjHLzhR/4etsZkZZ9Cg3HhPA5pjVcI5kJct4kLjWM+ejZliZoV/KvpJN261VKKLTJMX64UeMiLAlb7mUNoNqKztgflxz5Dbad5hemvgwg50=");

        BigInteger modules = new BigInteger(1, modBytes);
        BigInteger exponent = new BigInteger(1, expBytes);
        BigInteger d = new BigInteger(1, dBytes);

        KeyFactory factory = KeyFactory.getInstance("RSA");

        RSAPublicKeySpec pubSpec = new RSAPublicKeySpec(modules, exponent);
        this.pubKey = factory.generatePublic(pubSpec);

        RSAPrivateKeySpec privSpec = new RSAPrivateKeySpec(modules, d);
        this.priKey = factory.generatePrivate(privSpec);

    }

    public char[] encrypt(byte[] bytes) throws Exception {

        cipher.init(Cipher.ENCRYPT_MODE, pubKey);

        byte[] encrypted = blockCipher(bytes, cipher, Cipher.ENCRYPT_MODE);
        char[] encryptedTranspherable = Hex.encodeHex(encrypted);

        return encryptedTranspherable;
    }

    public byte[] decrypt(char[] bytes) throws Exception {

        cipher.init(Cipher.DECRYPT_MODE, priKey);
        byte[] bts = Hex.decodeHex(bytes);
        byte[] decrypted = blockCipher1(bts, cipher, Cipher.DECRYPT_MODE);

        return decrypted;
    }

    public char[] encodeHex(byte[] bytes) {
        char[] chars = Hex.encodeHex(bytes);
        return chars;
    }

    public byte[] decodeHex(char[] chars) {
        byte[] bytes = null;
        try {
            bytes = Hex.decodeHex(chars);
        } catch (Exception ex) {
            System.out.println("decodeHex|" + ex);
        }
        return bytes;
    }

    public byte[] getBytes(Object o) {

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(o);
        } catch (Exception ex) {
            System.out.println("BatchSecurityManager|getObjectByte|" + ex);
        }

        return out.toByteArray();
    }

    public Object getObject(byte[] bytes) {

        ByteArrayInputStream b = new ByteArrayInputStream(bytes);
        Object object = null;

        try {
            ObjectInputStream o = new ObjectInputStream(b);
            object = o.readObject();
        } catch (Exception ex) {
            System.out.println("BatchSecurityManager|getObjectByte|" + ex);
        }

        return object;
    }

    private byte[] blockCipher(byte[] bytes, Cipher cipher, int mode) throws IllegalBlockSizeException, BadPaddingException {

        byte[] scrambled = new byte[0];
        byte[] toReturn = new byte[0];

        int length = (mode == Cipher.ENCRYPT_MODE) ? 100 : 128;
        byte[] buffer = new byte[length];

        for (int i = 0; i < bytes.length; i++) {
            if ((i > 0) && (i % length == 0)) {
                scrambled = cipher.doFinal(buffer);
                toReturn = append(toReturn, scrambled);
                int newlength = length;

                if (i + length > bytes.length) {
                    newlength = bytes.length - i;
                }
                buffer = new byte[newlength];
            }
            buffer[i % length] = bytes[i];
        }
        scrambled = cipher.doFinal(buffer);
        toReturn = append(toReturn, scrambled);

        return toReturn;
    }

    private byte[] blockCipher1(byte[] bytes, Cipher cipher, int mode) throws IllegalBlockSizeException, BadPaddingException {

        byte[] scrambled = new byte[0];
        byte[] toReturn = new byte[0];

        int length = (mode == Cipher.ENCRYPT_MODE) ? 100 : 128;
        byte[] buffer = new byte[length];

        for (int i = 0; i < bytes.length; i++) {
            if ((i > 0) && (i % length == 0)) {
                scrambled = cipher.doFinal(buffer);
                toReturn = append(toReturn, scrambled);
                int newlength = length;
                if (i + length > bytes.length) {
                    newlength = bytes.length - i;
                }
                buffer = new byte[newlength];
            }
            buffer[i % length] = bytes[i];
        }

        scrambled = cipher.doFinal(buffer);
        toReturn = append(toReturn, scrambled);

        return toReturn;
    }

    private byte[] append(byte[] prefix, byte[] suffix) {

        byte[] toReturn = new byte[prefix.length + suffix.length];
        System.arraycopy(prefix, 0, toReturn, 0, prefix.length);
        System.arraycopy(suffix, 0, toReturn, prefix.length, suffix.length);

        return toReturn;
    }

    public char[] byteToChar(byte[] bytes) {

        char[] buffer = new char[bytes.length >> 1];
        for (int i = 0; i < buffer.length; i++) {
            int bpos = i << 1;
            char c = (char) (((bytes[bpos] & 0x00FF) << 8) + (bytes[bpos + 1] & 0x00FF));
            buffer[i] = c;
        }
        return buffer;
    }

    public byte[] charToByte(char[] buffer) {

        byte[] b = new byte[buffer.length - 1];
        for (int i = 0; i < b.length; i++) {
            b[i] = (byte) buffer[i];
        }
        return b;
    }
}
