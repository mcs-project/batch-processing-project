/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.client;

import com.batch.resource.CPUInfo;
import com.batch.type.ClientStatusType;
import org.hyperic.sigar.Sigar;

/**
 *
 * @author sujithde
 */
public class StatusManager extends Thread {

    @Override
    public void run() {
        System.out.println("status manger is starting ..... SUCCESS");
        while (true) {
            try {
                CPUInfo cpui = new CPUInfo(new Sigar());
                double idle = cpui.getCPUIDLE();

                if (ClientStatusType.status != ClientStatusType.IDLE) {
                    if (idle < 50) {
                        ClientStatusType.status = ClientStatusType.BUSY;
                    }
                } else {
                    break;
                }
                sleep(1000);
            } catch (Exception e) {
                System.out.println("StatusManager|run|" + e);
            }
        }
    }
}
