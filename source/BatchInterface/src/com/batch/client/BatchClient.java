/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.client;

import com.batch.server.BatchServerInterface;
import com.batch.util.Utility;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author sujithde
 */
public abstract class BatchClient extends UnicastRemoteObject implements BatchClientInterface {

    public BatchClient() throws RemoteException {
    }

    public BatchServerInterface getRMIServerInterface() throws Exception {
        BatchServerInterface rmisi = null;

        String name = "rmi://192.168.24.97:1099/batchproc";
        Utility utility=new Utility();
        String serverHost=utility.getServerHost();
        Registry registry = LocateRegistry.getRegistry(serverHost, 1099);
        Remote remoteObject = (BatchServerInterface) registry.lookup("batchproc");
       
        if (remoteObject instanceof BatchServerInterface) {
            rmisi = (BatchServerInterface) remoteObject;
        } else {
            System.out.println("server not a batch server.");
        }

        return rmisi;
    }
}
