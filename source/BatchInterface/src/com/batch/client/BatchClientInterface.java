package com.batch.client;

import com.batch.manage.BatchRequest;
import com.batch.manage.BatchResponse;
import com.batch.resource.ResourceObject;
import java.rmi.*;

public interface BatchClientInterface extends Remote {

    static final long serialVersionUID = 1L;

    public void setProcessLogic(BatchClientLogic pl) throws RemoteException;

    public void joinBatchProcess(String clientID) throws RemoteException;

    public boolean isIDLE() throws RemoteException;

    public String getIDLEStatus() throws RemoteException;

    public char[] processData(char[] batchRequest, int securityType) throws RemoteException;

    public BatchResponse processData(BatchRequest batchRequest, int securityType) throws RemoteException;

    public ResourceObject getResourceUtlizationObject() throws RemoteException;

    public void exitBatchProcess() throws RemoteException;

    public String getClientID() throws RemoteException;

    public void forceTerminate() throws RemoteException;

    public void setClientStatus(int status) throws RemoteException;
}
