/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.client;

import com.batch.manage.BatchRequest;
import com.batch.manage.BatchResponse;
import java.io.Serializable;

/**
 *
 * @author sujithde
 */
public abstract class BatchClientLogic implements Serializable{

    public abstract BatchResponse doProcess(BatchRequest batchRequest);
}
