/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.pdf.run;

import com.batch.client.BatchClientLogic;
import com.batch.manage.BatchRequest;
import com.batch.manage.BatchResponse;
import com.batch.manage.DataObject;
import com.batch.server.BatchServer;
import com.batch.server.BatchServerLogic;
import com.batch.type.AlgorithmType;
import com.batch.type.SecurityType;
import com.batch.util.Utility;
import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.BaseFont;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 *
 * @author sujithde
 */
public class PDFCreater implements Serializable {

    private static final long serialVersionUID = 1L;

    public static void main(String[] args) throws RemoteException, MalformedURLException {
        Utility utility = new Utility();
        System.setProperty("java.rmi.server.codebase", "http://" + utility.getServerHost() + ":8080/bt/BatchPDFImpl_1.jar");
        System.setProperty("java.security.policy", "./resource/policy.cfg");
        System.setSecurityManager(new RMISecurityManager());

        if (args.length == 2) {
            String algo = args[0].trim();
            String secType = args[1].trim();

            new PDFCreater().pdfProcess(algo, secType);
        } else {
            System.out.println("Set valid parameters(Algorithm and Security)");
            System.out.println("\nAlgorithm");
            System.out.println("-------------------");
            System.out.println("\tROUND_BOBIN   1");
            System.out.println("\tSTATIC        2");
            System.out.println("\tDYNAMIC       3");

            System.out.println("\nSecurity Type");
            System.out.println("-------------------");
            System.out.println("\tNONE          0");
            System.out.println("\tPARTIAL       1");
            System.out.println("\tFULL          2");
            System.out.println("");
            System.out.println("Program Exit.");
        }
    }

    public void pdfProcess(String algo, String secType) throws RemoteException, MalformedURLException {

        if ((algo.equals("1") || algo.equals("2") || algo.equals("3")) && (secType.equals("0") || secType.equals("1") || secType.equals("2"))) {
            
            BatchServerLogic batchServerLogic = new BatchServerLogic() {
                @Override
                public Collection<DataObject> preProcess() {
                    FileProcess fp = new FileProcess();
                    File fc = new File("./txt/");
                    Collection<DataObject> in = new ArrayList<DataObject>();

                    String[] fNames = fc.list();
                    System.out.println("No of files to process :- " + fNames.length);
                    for (String fname : fNames) {
                        String mataData = fp.readFile("./txt/" + fname);

                        TextProcess tp = new TextProcess();
                        Map params =
                                tp.strSplitter(mataData);
                        DataObject dos = new DataObject();
                        dos.setKey(fname);
                        dos.setValue(params);
                        in.add(dos);
                    }
                    return in;
                }

                @Override
                public boolean postProcess(Collection<DataObject> clctn) {

                    Collection<DataObject> cldo = clctn;
                    Iterator<DataObject> itdo = cldo.iterator();
                    int i = 0;
                    boolean done = false;

                    while (itdo.hasNext()) {
                        try {
                            DataObject dos = itdo.next();
                            byte[] bytes = (byte[]) dos.getValue();

                            String fname = dos.getKey();
                            String pdfName = fname.substring(0, fname.length() - 4);

                            FileOutputStream out = new FileOutputStream("./pdf/" + pdfName + ".pdf");

                            out.write(bytes);
                            i++;
                        } catch (Exception ex) {
                            Logger.getLogger(PDFCreater.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    done = true;
                    return done;
                }
            };
            BatchClientLogic batchClientLogic = new BatchClientLogic() {
                @Override
                public BatchResponse doProcess(BatchRequest br) {

                    Collection<DataObject> dos = br.getCol();
                    Collection<DataObject> out = new ArrayList<DataObject>();

                    Iterator<DataObject> itr = dos.iterator();
                    BatchResponse batchResponse = new BatchResponse();

                    FontFactory.register("./resource/consola.ttf",
                            "Manning");
                    FontFactory.getFont("Manning", BaseFont.CP1252, BaseFont.EMBEDDED);
                    FontFactory.register("./resource/consolab.ttf",
                            "Manning-Bold");
                    FontFactory.getFont("Manning-Bold", BaseFont.CP1252, BaseFont.EMBEDDED);

                    while (itr.hasNext()) {
                        try {
                            DataObject dataObject = itr.next();
                            Map params = (Map) dataObject.getValue();

                            String jasper = "./resource/mc_proj.jasper";

                            JasperPrint jasperPrint =
                                    JasperFillManager.fillReport(jasper, params,
                                    new JREmptyDataSource());

                            byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);
                            System.out.println(dataObject.getKey() + " ..... done");
                            dataObject.setValue(bytes);
                            out.add(dataObject);
                        } catch (JRException ex) {
                            Logger.getLogger(PDFCreater.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    batchResponse.setCol(out);
                    return batchResponse;
                }
            };

            BatchServer batchServer = new BatchServer();
            batchServer.startServer(Integer.parseInt(algo), Integer.parseInt(secType), batchServerLogic, batchClientLogic);
        } else {
            System.out.println("Set valid parameters(Algorithm and Security)");
            System.out.println("\nAlgorithm");
            System.out.println("-------------------");
            System.out.println("\tROUND_BOBIN   1");
            System.out.println("\tSTATIC        2");
            System.out.println("\tDYNAMIC       3");

            System.out.println("\nSecurity Type");
            System.out.println("-------------------");
            System.out.println("\tNONE          0");
            System.out.println("\tPARTIAL       1");
            System.out.println("\tFULL          2");
            System.out.println("");
            System.out.println("Program Exit.");
        }
    }
}
