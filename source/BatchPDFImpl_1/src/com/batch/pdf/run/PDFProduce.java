package com.batch.pdf.run;

import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.BaseFont;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Statement;
import java.sql.Time;
import java.util.Map;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class PDFProduce {

//    Logger logger;
//    Logger mlogger;
//
//    private class PDFThread extends Thread {
//
//        private String fname = null;
//
//        private PDFThread(String fileName) {
//            fname = fileName;
//        }
//
//        @Override
//        public void run() {
//            JasperPrint jasperPrint;
//            Connection con = null;
//            Statement st = null;
//
//            FileProcess fp = new FileProcess();
//
//            //reading .txt file for processing
//            String mataData = fp.readFile("./txt/" + fname);
//
//            TextProcess tp = new TextProcess();
//            Map params =
//                    tp.strSplitter(mataData);
//            String fend = fname.substring(fname.lastIndexOf(".") - 1, fname.lastIndexOf("."));
//
//            String jasper = "";
//            try {
//                String mobile = (String) params.get("mob_no");
//                String vatRegNo = (String) params.get("vat_reg_no");
//                String billDate = (String) params.get("bill_date");
//
//                if (fend.equalsIgnoreCase("D")) {
//
//                    if (!vatRegNo.equalsIgnoreCase("NULL")) {
//                        jasper =
//                                "./resources/jasper/bill_indi_detail_vat.jasper";
//                    } else {
//                        jasper =
//                                "./resources/jasper/bill_indi_detail.jasper";
//                    }
//                } else if (fend.equalsIgnoreCase("S")) {
//                    if (!vatRegNo.equalsIgnoreCase("NULL")) {
//                        jasper = "./resources/jasper/bill_indi_sum_vat.jasper";
//                    } else {
//                        jasper = "./resources/jasper/bill_indi_sum.jasper";
//                    }
//                }
//
//                //use jasper report for PDF creation
//                jasperPrint =
//                        JasperFillManager.fillReport(jasper, params,
//                        new JREmptyDataSource());
//                String flname = fname.substring(0, fname.lastIndexOf("."));
//
//                JasperExportManager.exportReportToPdfFile(jasperPrint,
//                        "./pdf/" + flname + ".pdf");
//
//                FClient fc = new FClient();
//
//                //store files in server
//                fc.putFile("./pdf/" + flname + ".pdf", flname + ".pdf");
//
//                Validate validate = new Validate();
//                DataBaseConnection dbc = new DataBaseConnection();
//
//                con = dbc.openConnection("ifx295");
//                st = con.createStatement();
//                String eEmail = validate.getEmail(mobile, con);
//
//                if (eEmail != null) {
//                    Email email = new Email();
//                    File file = new File("./pdf/" + flname + ".pdf");
//                    if (file.exists()) {
//                        email.sendEmail(eEmail, validate.createMessage(billDate, con), flname, billDate);
//                        mlogger.info(flname + "|sent");
//                    }
//                }
//
//                //move files to tmp location
//                fp.moveFile("./txt/" + flname + ".txt", "./oldtxt/" + flname + ".txt");
//                fp.moveFile("./pdf/" + flname + ".pdf", "./oldpdf/" + flname + ".pdf");
//
//                con.close();
//            } catch (Exception ex) {
//                try {
//                    con.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//    }
//
//    public static void main(String[] args) {
//
//        FileProcessor fp = new FileProcessor();
//
//        //clear temp files & set environment clean for start process.
//        fp.emptyDir("./oldpdf/");
//        System.out.println("Cleaned ./oldpdf directory");
//        fp.emptyDir("./oldtxt/");
//        System.out.println("Cleaned ./oldtxt directory");
//
//        try {
//            new PDFProduce();
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    public PDFProduce() throws IOException {
//
//        //register consolas regular & bold font
//        FontFactory.register("./resources/fonts/consola.ttf",
//                "Manning");
//        FontFactory.getFont("Manning", BaseFont.CP1252, BaseFont.EMBEDDED);
//        FontFactory.register("./resources/fonts/consolab.ttf",
//                "Manning-Bold");
//        FontFactory.getFont("Manning-Bold", BaseFont.CP1252, BaseFont.EMBEDDED);
//
//        PropertyConfigurator.configure("./config/logconfig.txt");
//        logger = Logger.getLogger("debugl");
//        mlogger = Logger.getLogger("mail");
//
//        //list files in ./txt directory
//        File fc = new File("./txt/");
//
//        String[] fNames = fc.list();
//        System.out.println("No of files to process :- " + fNames.length);
//        for (String fname : fNames) {
//            //one .txt file processed with one thread
//            //maximum 25 thread will execute when running the app
//            while (PDFThread.activeCount() > 25) {
//                try {
//                    Thread.sleep(10);
//                } catch (InterruptedException ex) {
//                    ex.printStackTrace();
//                }
//            }
//            (new PDFThread(fname)).start();
//        }
//    }
}
