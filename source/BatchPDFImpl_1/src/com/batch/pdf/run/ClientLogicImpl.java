/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.pdf.run;

import com.batch.client.BatchClientLogic;
import com.batch.manage.BatchRequest;
import com.batch.manage.BatchResponse;
import com.batch.manage.DataObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 *
 * @author sujithde
 */
public class ClientLogicImpl extends BatchClientLogic  implements Serializable{
    
    @Override
    public BatchResponse doProcess(BatchRequest br) {
        System.out.println("ClientLogicImpl|doProcess|");
        Collection<DataObject> dos = br.getCol();
        Collection<DataObject> out = new ArrayList<DataObject>();

        Iterator<DataObject> itr = dos.iterator();
        BatchResponse batchResponse = new BatchResponse();
        while (itr.hasNext()) {
            try {
                DataObject dataObject = itr.next();
                Map params = (Map) dataObject.getValue();

                String jasper = "./resource/mc_proj.jasper";
                System.out.println("jasper :- " + jasper);
                JasperPrint jasperPrint =
                        JasperFillManager.fillReport(jasper, params,
                        new JREmptyDataSource());


                byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);

                dataObject.setValue(bytes);
                out.add(dataObject);
            } catch (JRException ex) {
                Logger.getLogger(PDFCreater.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        batchResponse.setCol(out);
        return batchResponse;
    }
}
