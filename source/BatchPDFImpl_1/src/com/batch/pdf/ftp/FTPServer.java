/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.pdf.ftp;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;

/**
 *
 * @author sujithde
 */
public class FTPServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            FtpServerFactory serverFactory = new FtpServerFactory();

            ListenerFactory factory = new ListenerFactory();

    // set the port of the listener
            factory.setPort(2221);

    // replace the default listener
            serverFactory.addListener("default", factory.createListener());
           
            PropertiesUserManagerFactory userManagerFactory = new PropertiesUserManagerFactory();
userManagerFactory.setFile(new File("C:\\Users\\sujithde\\Documents\\NetBeansProjects\\BatchPDFSystem\\src\\com\\batch\\ftp\\users.properties"));
        
serverFactory.setUserManager(userManagerFactory.createUserManager());
    // start the server
            FtpServer server = serverFactory.createServer();

            server.start();
        } catch (FtpException ex) {
            ex.printStackTrace();
            Logger.getLogger(FTPServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
