/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.pdf.run;

import com.batch.manage.DataObject;
import com.batch.server.BatchServerLogic;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sujithde
 */
public class ServerLogicImpl extends BatchServerLogic implements Serializable {

    @Override
    public Collection<DataObject> preProcess() {
        TextProcess tp = new TextProcess();
        FileProcess fp = new FileProcess();
        Collection<DataObject> in = new ArrayList<DataObject>();

        String file = "./txt/74522091-453745D.txt";
        String str = fp.readFile(file);

        Collection<HashMap> colMap = tp.strSplitter(file, str);
        Iterator<HashMap> itrMap = colMap.iterator();

        while (itrMap.hasNext()) {
            HashMap params = (HashMap) itrMap.next();
            DataObject dos = new DataObject();
            dos.setValue(params);
            in.add(dos);
        }
        return in;
    }

    @Override
    public boolean postProcess(Collection<DataObject> clctn) {
        Collection<DataObject> cldo = clctn;
        Iterator<DataObject> itdo = cldo.iterator();
        int i = 0;
        boolean done = false;
        while (itdo.hasNext()) {
            try {
                DataObject dos = itdo.next();
                byte[] bytes = (byte[]) dos.getValue();

                FileOutputStream out = new FileOutputStream("data" + i + ".pdf");

                out.write(bytes);
                i++;
                done = true;
            } catch (IOException ex) {
                Logger.getLogger(PDFCreater.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return done;
    }
}
