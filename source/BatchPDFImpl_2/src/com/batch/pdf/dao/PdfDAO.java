/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.batch.pdf.dao;

/**
 *
 * @author sujithde
 */
public class PdfDAO {

private String pdfCount;
private String pdfDesc;

    public String getPdfCount() {
        return pdfCount;
    }

    public void setPdfCount(String pdfCount) {
        this.pdfCount = pdfCount;
    }

    public String getPdfDesc() {
        return pdfDesc;
    }

    public void setPdfDesc(String pdfDesc) {
        this.pdfDesc = pdfDesc;
    }
}
