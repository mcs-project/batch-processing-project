package com.batch.pdf.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

public class FileProcess {

    public String readFile(String file) {

        StringBuilder contents = new StringBuilder();

        try {

            BufferedReader input = new BufferedReader(new FileReader(file));
            try {
                String line = null; 

                while ((line = input.readLine()) != null) {
                    contents.append(line);
                    contents.append(System.getProperty("line.separator"));
                }
            } finally {
                input.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return contents.toString();

    }

    public File[] getFileList(String path) {
        File file = new File(path);
        File[] files = file.listFiles();
        return files;
    }

    public void deleteFile(String file) {
        File fe = new File(file);
        if (fe.exists()) {            
            fe.delete();
        }
    }

    public boolean isExist(Vector files) {
        boolean exist = true;
        for (int i = 0; i < files.size(); i++) {
            File fe = new File("./pdf/" + files.get(i).toString());

            if (!fe.canRead()) {
                exist = false;
                break;
            }
        }
        return exist;
    }
}
