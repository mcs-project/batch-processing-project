/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.pdf.run;

import com.batch.pdf.file.FileProcess;
import com.lowagie.text.Document;
import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author sujithde
 */
public class Merger {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        if (args.length > 0) {
            System.out.println("Merging PDF files ..... ");
            FileProcess fp = new FileProcess();
            File file[] = fp.getFileList("./pdf/");

            ArrayList fname = new ArrayList();
            for (int i = 0; i < file.length; i++) {
                fname.add(file[i].getName());
            }
            
            System.setProperty("java.security.policy", "./resource/policy.cfg");

            java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
            Time time = new Time(System.currentTimeMillis());
            System.out.println("Merging Started on " + date + " at " + time + " (" + System.currentTimeMillis() + ")");

            mergePDF(fname, args[0]);
            System.out.println("Merging Finished on " + date + " at " + time + " (" + System.currentTimeMillis() + ")");
        } else {
            System.out.println("Specify destination file name");
        }
    }

    public static void mergePDF(ArrayList al, String dfile) {
        try {

            List<InputStream> pdfs = new ArrayList<InputStream>();

            for (int i = 0; i < al.size(); i++) {
                pdfs.add(new FileInputStream("./pdf/" + al.get(i).toString()));
            }

            OutputStream output = new FileOutputStream("./pdf/" + dfile + ".pdf");
            concatPDFs(pdfs, output, true);
            
            FileProcess fp = new FileProcess();
            for (int i = 0; i < al.size(); i++) {
                fp.deleteFile("pdf/" + al.get(i).toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void concatPDFs(List<InputStream> streamOfPDFFiles, OutputStream outputStream, boolean paginate) {

        Document document = new Document();
        try {
            List<InputStream> pdfs = streamOfPDFFiles;
            List<PdfReader> readers = new ArrayList<PdfReader>();
            int totalPages = 0;
            Iterator<InputStream> iteratorPDFs = pdfs.iterator();

            while (iteratorPDFs.hasNext()) {
                InputStream pdf = iteratorPDFs.next();
                PdfReader pdfReader = new PdfReader(pdf);
                readers.add(pdfReader);
                totalPages += pdfReader.getNumberOfPages();
            }

            PdfWriter writer = PdfWriter.getInstance(document, outputStream);

            document.open();
            FontFactory.register("./resource/consola.ttf",
                    "Manning");

            BaseFont bf = BaseFont.createFont(BaseFont.COURIER,
                    BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

            PdfContentByte cb = writer.getDirectContent();

            PdfImportedPage page;
            int currentPageNumber = 0;
            int pageOfCurrentReaderPDF = 0;
            Iterator<PdfReader> iteratorPDFReader = readers.iterator();

            while (iteratorPDFReader.hasNext()) {
                PdfReader pdfReader = iteratorPDFReader.next();

                while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {
                    document.newPage();
                    pageOfCurrentReaderPDF++;
                    currentPageNumber++;
                    page = writer.getImportedPage(pdfReader, pageOfCurrentReaderPDF);
                    cb.addTemplate(page, 0, 0);

                    if (paginate) {
                        cb.beginText();
                        cb.setFontAndSize(bf, 9);
                        cb.showTextAligned(PdfContentByte.ALIGN_CENTER, "" + currentPageNumber + " of " + totalPages, 520, 5, 0);
                        cb.endText();
                    }
                }
                pageOfCurrentReaderPDF = 0;
            }
            outputStream.flush();
            document.close();
            outputStream.close();
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        } finally {
            if (document.isOpen()) {
                document.close();
            }
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
}
