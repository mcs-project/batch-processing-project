/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batch.client;

import com.batch.manage.BatchRequest;
import com.batch.manage.BatchResponse;
import com.batch.resource.CPUInfo;
import com.batch.resource.IPInfo;
import com.batch.resource.MemoryInfo;
import com.batch.resource.ResourceObject;
import com.batch.sec.BatchSecurityManager;
import com.batch.server.BatchServerInterface;
import com.batch.type.ClientStatusType;
import com.batch.type.SecurityType;
import com.batch.type.ServerStatusType;
import com.batch.util.OSUtility;
import com.batch.util.Utility;
import java.io.Serializable;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.hyperic.sigar.Sigar;

public class BatchClientImpl extends BatchClient implements Serializable {

//    private static boolean SLEEP = false;
//    public static boolean IDLE = true;
    BatchClientLogic pl = null;
    Logger logger = null;

    public BatchClientImpl() throws RemoteException {
        try {
            logger = Logger.getLogger("log4j");
            PropertyConfigurator.configure("./resource/logconfig.cfg");
        } catch (Exception e) {
            System.out.println("FTPServer|" + e);
        }
    }

    @Override
    public void joinBatchProcess(String myID) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public char[] processData(char[] br, int securityType) throws RemoteException {
        BatchResponse batchResponse = new BatchResponse();
        BatchSecurityManager bsm = null;
        char[] response = null;
        try {
            if (securityType == SecurityType.FULL) {

                bsm = new BatchSecurityManager();
                Object object = bsm.getObject(bsm.decrypt(br));
                BatchRequest batchRequest = (BatchRequest) object;
                System.out.println("Dynamic Batch size ..... " + batchRequest.getCol().size());
                System.out.println("Batch ID " + batchRequest.getKey() + " started processing .....");
                batchResponse = pl.doProcess(batchRequest);
                response = bsm.encrypt(bsm.getBytes(batchResponse));
                System.out.println("Batch ID " + batchRequest.getKey() + " processing ..... SUCCESS");

            } else if (securityType == SecurityType.PARTIAL) {

                bsm = new BatchSecurityManager();
                Object object = bsm.getObject(bsm.decodeHex(br));
                BatchRequest batchRequest = (BatchRequest) object;
                System.out.println("Batch ID " + batchRequest.getKey() + " started processing .....");
                batchResponse = pl.doProcess(batchRequest);
                response = bsm.encodeHex(bsm.getBytes(batchResponse));
                System.out.println("Batch ID " + batchRequest.getKey() + " processing ..... SUCCESS");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("BatchClientImpl|BatchSecurityManager|" + ex);
        }
        return response;
    }

    @Override
    public BatchResponse processData(BatchRequest batchRequest, int securityType) throws RemoteException {
        BatchResponse batchResponse = new BatchResponse();
        try {
            if (securityType == SecurityType.NONE) {
                System.out.println("Batch ID " + batchRequest.getKey() + " started processing .....");
                batchResponse = pl.doProcess(batchRequest);
                System.out.println("Batch ID " + batchRequest.getKey() + " processing ..... SUCCESS");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("BatchClientImpl|BatchSecurityManager|" + ex);
        }
        return batchResponse;
    }

    @Override
    public void exitBatchProcess() throws RemoteException {
        System.out.println("Batch Processing finished ..... SUCCESS");
        System.exit(0);
    }

    @Override
    public String getClientID() throws RemoteException {
        Utility utility = new Utility();
        String ip = utility.getIPAddress();
        return ip;
    }

    @Override
    public void setProcessLogic(BatchClientLogic pl) throws RemoteException {
        this.pl = pl;
        ClientStatusType.status = ClientStatusType.NOTACTIVE;
    }

    public static void main(String[] args) {

        System.out.println("Initializing client environment...");

        System.out.print("Loading client Policy file ..... ");
        System.setProperty("java.security.policy", "./resource/policy.cfg");
        System.out.println("SUCCESS");
        System.out.print("Setting up the Security Manager ..... ");
        System.setSecurityManager(new RMISecurityManager());
        System.out.println("SUCCESS");
        System.out.print("Retrieving Batch Server IP ..... ");
        Utility utility = new Utility();
        String serverHost = utility.getServerHost();
        System.out.println(serverHost);

        System.out.print("Retrieving machine Operating System ..... ");
        OSUtility osu = new OSUtility();

        if (osu.isUnix()) {
            System.out.println("Linux");
        } else if (osu.isWindows()) {
            System.out.println("Windows");
        }

        System.out.print("Retrieving client IP address ..... ");
        IPInfo info = new IPInfo();
        String ip = null;

        if (osu.isUnix()) {
            ip = info.getLinuxIPAddress();
            System.out.println(ip);
        } else if (osu.isWindows()) {
            ip = info.getWindowsIPAddress().getHostAddress();
            System.out.println(ip);
        }
        System.setProperty("java.rmi.server.hostname", ip);
        System.out.println("Initializing ..... SUCCESS");

        BatchClientImpl bci = null;
        BatchServerInterface bsi = null;
        boolean waiting = false;
        boolean srunner = false;

        while (true) {
            try {
                bci = new BatchClientImpl();
                while (true) {
                    try {
                        bsi = bci.getRMIServerInterface();
                        if (bsi != null) {
                            if (bsi.isServerActive()) {
                                if (!srunner) {
                                    StatusManager sm = new StatusManager();
                                    sm.start();
                                    srunner = true;
                                }
                                waiting = false;
                                break;
                            }
                        }
                    } catch (Exception e) {
                        if (bsi == null) {
                            if (!waiting) {
                                System.out.println();
                                System.out.println("Waiting for Batch Server...");
                                System.out.println();
                                waiting = true;
                            }
                        } else {
                            bsi = null;
                        }
                    }
                }

                ClientStatusType.status = ClientStatusType.IDLE;

                while (true) {
                    int serverStatus = bsi.getServerStatus();
                    if (ClientStatusType.status == ClientStatusType.IDLE && serverStatus != ServerStatusType.FINISH) {
                        int bsize = bsi.getBatchSize();
                        if (bsize > 0) {
                            String joinRequest = bsi.joinRequest(bci);
                            System.out.println(joinRequest);
                            bsi.joinIDLE(bci);
                        }
                        break;
                    }
                }

                while (true) {
                    int serverStatus = bsi.getServerStatus();
                    if (serverStatus == ServerStatusType.FINISH) {
                        break;
                    }
                }

            } catch (Exception e) {
                waiting = false;
            }
        }
    }

    @Override
    public void forceTerminate() throws RemoteException {
        System.out.println("Force terminated by server.");
        System.exit(0);
    }

    @Override
    public boolean isIDLE() throws RemoteException {
        if (ClientStatusType.status == ClientStatusType.IDLE) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String getIDLEStatus() throws RemoteException {
        String status = null;
        try {
            CPUInfo cpui = new CPUInfo(new Sigar());
            MemoryInfo mi = new MemoryInfo(new Sigar());
            status = cpui.getCPUIDLE() + "%, " + mi.getFreeMemory();
        } catch (Exception e) {
            System.out.println(e);
        }
        return status;
    }

    @Override
    public ResourceObject getResourceUtlizationObject() throws RemoteException {
        ResourceObject ro = new ResourceObject();
        try {
            CPUInfo cPUInfo = new CPUInfo(new Sigar());
            ro.setCpuCache(cPUInfo.getCPUCacheSize());
            ro.setNoCPU(cPUInfo.getNoCPU());
            ro.setCpuIDLE(cPUInfo.getCPUIDLE());

            MemoryInfo memoryInfo = new MemoryInfo(new Sigar());
            ro.setFreeMemory(memoryInfo.getFreeMemory());
            ro.setTotalMemory(memoryInfo.getTotalMemory());
            ro.setFreeMemoryPercent(memoryInfo.getFreeMemoryPercent());

        } catch (Exception ex) {
            System.out.println("getResourceUtlizationObject|" + ex);
        }
        return ro;
    }

    @Override
    public void setClientStatus(int i) throws RemoteException {
        ClientStatusType.status = i;
    }
}
